#### TLM Parser

Hi! This is a simple parser that I wrote to format an LS quiz with TLM.
There are a few conventions that are expected when using this parser, so please
keep these in mind.

1. Use ordered lists when writing your quiz questions. The parser doesn't account
for text before the quiz questions, so when you do want to use this parser, make
sure the first thing in the file is the first question.
2. This parser assumes correct indentation. If you want to nest something under
a question then use 4 spaces to do so or two soft tabs. Make sure that your
content is lined up correctly, or else the end result may not be correctly formatted.
3. The parser relies on certain lines to format your quiz. Make sure that you use
check boxes in your questions and have **Correct** as the start of your discussion
section.


This script will create a folder for the course you are working on, and a markdown file
within that folder in the following format:

"course_X/lesson_Y_quiz_X.markdown"

Where X, Y, and Z is the course number, lesson number, and quiz number respectively.

**USAGE:**

`tlm_parser/convert_to_tlm $COURSE_NUMBER $LESSON_NUMBER $QUIZ_NUMBER $FILE_TO_CONVERT`

*or just*

`./convert_to_tlm $COURSE_NUMBER $LESSON_NUMBER $QUIZ_NUMBER $FILE_TO_CONVERT`

*If you have moved the executable elsewhere.*

Please let me know if you find any bugs. I'll try and fix them as soon as I can.
